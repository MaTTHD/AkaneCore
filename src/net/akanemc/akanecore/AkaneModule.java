package net.akanemc.akanecore;

import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created by Matthew on 2015-12-04.
 */
public abstract class AkaneModule {

    public abstract void onEnable();
    public abstract void onDisable();
}
