package net.akanemc.akanecore.gameapi.game;

/**
 * Created by Matthew on 2015-10-10.
 */
public enum GameState {

    INGAME, LOBBY, DISABLED;
}
