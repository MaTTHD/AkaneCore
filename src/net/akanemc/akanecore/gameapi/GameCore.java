package net.akanemc.akanecore.gameapi;


import net.akanemc.akanecore.AkaneModule;
import net.akanemc.akanecore.gameapi.game.ArcadeGame;
import net.akanemc.akanecore.gameapi.game.GameMap;
import net.akanemc.akanecore.gameapi.kit.Kit;

/**
 * Created by Matthew on 2015-10-10.
 */
public abstract class GameCore extends AkaneModule {

    public abstract ArcadeGame setupGame();

    public abstract Kit[] registerKits();

    public abstract GameMap[] registerMaps();
}
