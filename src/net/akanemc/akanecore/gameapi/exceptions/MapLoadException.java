package net.akanemc.akanecore.gameapi.exceptions;

/**
 * Created by Matthew on 2015-10-11.
 */
public class MapLoadException extends Exception {

    public MapLoadException(String exception) {
        super(exception);
    }
}
