package me.xxmatthdxx.nebular.cmds;

import me.xxmatthdxx.nebular.rank.RankManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

/**
 * Created by Matt on 15/07/2015.
 */
public class MultiplierCommand implements CommandExecutor {

    /**
     *
     *
     * Basic multiplier command, allows admins to take, set, and add currency to a players cache.
     *
     * @param sender Returns the sender of the command
     *
     * @param cmd Returns the command being executed
     *
     * @param s Returns the command in String form
     *
     * @param args Returns the arguments passed, or done within the command
     * @return
     *
     *
     **/

    public boolean onCommand(CommandSender sender, Command cmd, String s, String[] args) {
        if(cmd.getName().equalsIgnoreCase("multiplier")){
            if(!(sender instanceof ConsoleCommandSender) || !(RankManager.hasAdminRank((Player)sender))){
                return true;
            }

            if(args.length != 1){
                return true;
            }

            try {
                int multiplier = Integer.valueOf(args[0]);
                //TODO network multiplier coming in version 0.2
            } catch (NumberFormatException e){
                e.printStackTrace();
            }
        }
        return false;
    }
}
