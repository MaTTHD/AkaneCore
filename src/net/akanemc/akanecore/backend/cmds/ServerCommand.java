package me.xxmatthdxx.nebular.cmds;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import me.xxmatthdxx.nebular.Nebular;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by Matt on 02/06/2015.
 */
public class ServerCommand implements CommandExecutor {

    /**
     *
     *
     * Basic server command, acting as a wrapper for /server hub.
     *
     * @param sender Returns the sender of the command
     *
     * @param cmd Returns the command being executed
     *
     * @param s Returns the command in String form
     *
     * @param args Returns the arguments passed, or done within the command
     * @return
     *
     *
     **/

    public boolean onCommand(CommandSender sender, Command cmd, String s, String[] args) {
        if(cmd.getName().equalsIgnoreCase("leave")){

            if(!(sender instanceof Player)){
                sender.sendMessage("Error: Console cannot change servers");
                return true;
            }

            Player player = (Player) sender;

            if(args.length > 0){

                return true;
            }

            if(Bukkit.getServer().getServerName().contains("Hub") || Bukkit.getServer().getServerName().contains("hub")){
                player.sendMessage(ChatColor.RED + "You cannot leave the hub!");
                return true;
            }
            else {

                String hub = "hub";
                ByteArrayDataOutput out = ByteStreams.newDataOutput();
                out.writeUTF("Connect");
                out.writeUTF(hub);
                player.sendPluginMessage(Nebular.getPlugin(), "BungeeCord", out.toByteArray());
            }
        }
        return false;
    }
}
