package me.xxmatthdxx.nebular.cmds;

import me.xxmatthdxx.nebular.Nebular;
import me.xxmatthdxx.nebular.player.PlayerManager;
import me.xxmatthdxx.nebular.rank.Rank;
import me.xxmatthdxx.nebular.rank.RankManager;
import me.xxmatthdxx.nebular.utils.UUIDFetcher;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.UUID;

/**
 * Created by Matt on 29/05/2015.
 */
public class RankCommand implements CommandExecutor {
	
	
	

    /**
     *
     *
     * Basic rank command, allows admins to change the rank of the specified player.
     *
     * @param sender Returns the sender of the command
     *
     * @param cmd Returns the command being executed
     *
     * @param s Returns the command in String form
     *
     * @param args Returns the arguments passed, or done within the command
     * @return
     *
     *
     **/

    public boolean onCommand(CommandSender sender, Command cmd, String s, final String[] args) {
        if (cmd.getName().equalsIgnoreCase("setrank")) {
            if (sender instanceof Player) {
                final Player pl = (Player) sender;
                if (!(RankManager.getInstance().hasAdminRank(pl))) {
                    pl.sendMessage(ChatColor.RED + "You do not have permission!");
                    return true;
                } else {
                    if (args.length != 2) {
                        pl.sendMessage(ChatColor.RED + "Improper usage: /setrank [player] [rank]");
                    } else {
                        final Rank rank = RankManager.getInstance().getRankFromName(args[1]);
                        if (rank == null) {
                            pl.sendMessage(Nebular.staff + ChatColor.RED + "No rank with that name!");
                            return true;
                        } else {
                            Player target = Bukkit.getPlayer(args[0]);
                            if (target == null) {
                                new BukkitRunnable() {
                                    public void run() {
                                        try {
                                            UUID uuid = UUIDFetcher.getUUIDOf(args[0]);
                                            RankManager.getInstance().setRank(uuid, rank);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }.runTaskAsynchronously(Nebular.getPlugin());
                                pl.sendMessage(Nebular.color(Nebular.staff + "&e" + args[0] + " &cis currently not online, but their rank has been updated."));
                                return true;
                            }
                            else {
                                PlayerManager.getInstance().getPlayer(target).setRank(rank);
                                PlayerManager.getInstance().getPlayer(target).setRankName(RankManager.getInstance().getFullName(rank));
                                RankManager.getInstance().setRank(target , rank);
                                pl.sendMessage(Nebular.color(Nebular.staff + "&a" + args[0] + "&e's rank has been set to " + rank.getColor() + rank.getName()));
                                return true;
                            }
                        }
                    }
                }
            }
            else {
                if (args.length != 2) {
                    sender.sendMessage(ChatColor.RED + "/setrank playername [rank]");
                } else {
                    final Rank rank = RankManager.getInstance().getRankFromName(args[1]);
                    if (rank == null) {
                        sender.sendMessage(ChatColor.RED + "No rank with that name!");
                        return true;
                    } else {
                        Player target = Bukkit.getPlayer(args[0]);
                        if (target == null) {

                            new BukkitRunnable() {
                                public void run() {
                                    try {

                                        UUID uuid = UUIDFetcher.getUUIDOf(args[0]);
                                        RankManager.getInstance().setRank(uuid, rank);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }.runTaskAsynchronously(Nebular.getPlugin());
                            sender.sendMessage(ChatColor.GREEN + "Cached player: " + args[0] + "'s rank as they are not online!");
                            return true;
                        }
                        else {
                            PlayerManager.getInstance().getPlayer(target).setRank(rank);
                            PlayerManager.getInstance().getPlayer(target).setRankName(RankManager.getInstance().getFullName(rank));
                            RankManager.getInstance().setRank(target , rank);
                            sender.sendMessage(ChatColor.GREEN + "set player: " + args[0] + "'s rank to: " + rank.name() + "!");
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }
}
