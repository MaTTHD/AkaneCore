package me.xxmatthdxx.nebular.utils;

import me.xxmatthdxx.nebular.player.NPlayer;
import me.xxmatthdxx.nebular.player.PlayerManager;
import me.xxmatthdxx.nebular.server.ServerManager;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.*;

/**
 * Created by Matt on 20/06/2015.
 */
public class ScoreboardUtils {
	
	
	public static void setHubScoreboard(Player p) {
        ScoreboardManager manager = Bukkit.getScoreboardManager();
        final Scoreboard board = manager.getNewScoreboard();

        final Objective objective = board.registerNewObjective("Nebular", "Nebular");
        objective.setDisplaySlot(DisplaySlot.SIDEBAR);
        objective.setDisplayName(ChatColor.YELLOW + "" + ChatColor.BOLD + "● HUB ●");


        Score top = objective.getScore(" ");
        Score score1 = objective.getScore(ChatColor.DARK_PURPLE + "" + ChatColor.BOLD + "Total Players:");
        Score score2 = objective.getScore(ChatColor.GOLD + "" + ServerManager.getInstance().getAllOnlinePlayers());
        Score space = objective.getScore("    ");
        Score score3 = objective.getScore(ChatColor.DARK_PURPLE + "" + ChatColor.BOLD + "Server:");
        Score score4 = objective.getScore(ChatColor.GOLD + PlayerManager.getInstance().getPlayer(p).getCurrentServer());
        Score space1 = objective.getScore("  ");
        Score score5 = objective.getScore(ChatColor.DARK_PURPLE + "" + ChatColor.BOLD + "Rank:");
        Score score6 = objective.getScore(ChatColor.GOLD + PlayerManager.getInstance().getPlayer(p).getRankName());
        Score space2 = objective.getScore("        ");
        Score score7 = objective.getScore(ChatColor.DARK_PURPLE + "" + ChatColor.BOLD + "Coins:");
        Score score8 = objective.getScore(ChatColor.GOLD + "" + PlayerManager.getInstance().getCurrency(p));
        Score space3 = objective.getScore("      ");
        Score score9 = objective.getScore(ChatColor.DARK_PURPLE + "" + ChatColor.BOLD + "Website:");
        Score score10 = objective.getScore(ChatColor.GOLD + "www.minetrap.com");

        top.setScore(15);
        score1.setScore(14);
        score2.setScore(13);
        space.setScore(12);
        score3.setScore(11);
        score4.setScore(10);
        space1.setScore(9);
        score5.setScore(8);
        score6.setScore(7);
        space2.setScore(6);
        score7.setScore(5);
        score8.setScore(4);
        space3.setScore(3);
        score9.setScore(2);
        score10.setScore(1);

        p.setScoreboard(board);
		
	}
	
	
    public static void setLobbyScoreboard(Player p) {

    	

    	
        ScoreboardManager manager = Bukkit.getScoreboardManager();
        final Scoreboard board = manager.getNewScoreboard();

        final Objective objective = board.registerNewObjective("Nebular", "Nebular");
        objective.setDisplaySlot(DisplaySlot.SIDEBAR);
        objective.setDisplayName(ChatColor.YELLOW + "" + ChatColor.BOLD + "● LOBBY ●");


        Score top = objective.getScore(" ");
        Score score1 = objective.getScore(ChatColor.DARK_PURPLE + "" + ChatColor.BOLD + "Coins:");
        Score score2 = objective.getScore(ChatColor.GOLD + "" + PlayerManager.getInstance().getCurrency(p));
        Score space = objective.getScore("    ");
        Score score3 = objective.getScore(ChatColor.DARK_PURPLE + "" + ChatColor.BOLD + "Players:");
        Score score4 = objective.getScore(ChatColor.GOLD + "" + Bukkit.getOnlinePlayers().size() + "/" + Bukkit.getMaxPlayers());
        Score space1 = objective.getScore("  ");
        Score score5 = objective.getScore(ChatColor.DARK_PURPLE + "" + ChatColor.BOLD + "Kills:");
        Score score6 = objective.getScore(ChatColor.GOLD + ""); //TODO GET TOTAL KILLS
        Score space2 = objective.getScore("        ");
        Score score7 = objective.getScore(ChatColor.DARK_PURPLE + ""+ ChatColor.BOLD + "Wins:");
        Score score8 = objective.getScore(ChatColor.GOLD + "");//TODO GET TOTAL WINS
        Score space3 = objective.getScore("      ");
        Score score9 = objective.getScore(ChatColor.DARK_PURPLE + ""+ ChatColor.BOLD + "Level:");
        Score score10 = objective.getScore(ChatColor.GOLD + ""); //TODO GET LEVEL

        top.setScore(15);
        score1.setScore(14);
        score2.setScore(13);
        space.setScore(12);
        score3.setScore(11);
        score4.setScore(10);
        space1.setScore(9);
        score5.setScore(8);
        score6.setScore(7);
        space2.setScore(6);
        score7.setScore(5);
        score8.setScore(4);
        space3.setScore(3);
        score9.setScore(2);
        score10.setScore(1);

        p.setScoreboard(board);
    }

    
}


