package me.xxmatthdxx.nebular.rank;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.UUID;

import me.xxmatthdxx.nebular.Nebular;
import me.xxmatthdxx.nebular.player.NPlayer;
import me.xxmatthdxx.nebular.player.PlayerManager;

import org.bukkit.entity.Player;

/**
 * Created by Matt on 21/04/2015.
 */

public class RankManager {

    private static RankManager instance = new RankManager();

    public static RankManager getInstance() {
        return instance;
    }

    /**
     * Sets a players rank
     * @param pl player to change rank
     * @param rank rank to give the player
     */
    public void setRank(Player pl, Rank rank) {
        Statement s = null;

        Rank plRank = getRank(pl);

        try (Connection con = Nebular.getPlugin().getSql().openConnection()) {
            s = con.createStatement();

            if (plRank != null) {
                s.executeUpdate("UPDATE playerData SET rank='" + rank.getName() + "' WHERE uuid='" + pl.getUniqueId() + "'");
            } else {
                s.executeUpdate("INSERT INTO playerData (`uuid`, `rank`) VALUES ('" + pl.getUniqueId() + "', '" + rank.getName() + "');");
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        System.out.println("Inserted info");
        PlayerManager.getInstance().getPlayer(pl).setRank(rank);
        PlayerManager.getInstance().getPlayer(pl).setRankName(getFullName(rank));
    }


    /**
     * Sets a uuid's rank
     * @param uuid uuid to change rank
     * @param rank rank to set
     */
    public void setRank(UUID uuid, Rank rank) {
        Statement s = null;
        Rank plRank = getRank(uuid);

        try (Connection con = Nebular.getPlugin().getSql().openConnection()) {
            s = con.createStatement();

            if (plRank != null) {
                s.executeUpdate("UPDATE playerData SET rank='" + rank.getName() + "' WHERE uuid='" + uuid + "'");
            } else {
                s.executeUpdate("INSERT INTO playerData (`uuid`, `rank`) VALUES ('" + uuid + "', '" + rank.getName() + "');");
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        System.out.println("Inserted info");
    }


    /**
     *
     * Gets the rank of a player
     * @param pl player to get rank
     * @return Rank of player
     */
    public Rank getRank(Player pl) {
        Statement s = null;
        ResultSet res = null;

        try (Connection con = Nebular.getPlugin().getSql().openConnection()) {
            s = con.createStatement();

            res = s.executeQuery("SELECT `rank` as r FROM `playerData` WHERE uuid='" + pl.getUniqueId() + "'");
            if (res.next()) {
                if (res.getString("r") == null) {
                    return null;
                } else {
                    return getRankFromName(res.getString("r"));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }


    /**
     * get rank of a uuid
     * @param uuid uuid to get rank
     * @return Rank of player
     */
    public Rank getRank(UUID uuid) {
        Statement s = null;
        ResultSet res = null;

        try (Connection con = Nebular.getPlugin().getSql().openConnection()) {
            s = con.createStatement();

            res = s.executeQuery("SELECT `rank` as r FROM `playerData` WHERE uuid='" + uuid + "'");
            if (res.next()) {
                if (res.getString("r") == null) {
                    return null;
                } else {
                    return getRankFromName(res.getString("r"));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }


    /**
     * Gets a rank from a name
     * @param name rank name to find
     * @return rank value of name
     */
    public Rank getRankFromName(String name) {
        for (Rank rank : Rank.values()) {
            if (rank.name.equalsIgnoreCase(name)) {
                return rank;
            }
            if(name.equalsIgnoreCase("default")){
                return Rank.DEFAULT;
            }
        }
        return null;
    }

    /**
     * Gets if player is a donator
     * @param pl player to check
     * @return true or false
     */
    public static boolean hasVipRank(Player pl) {
        NPlayer splayer = PlayerManager.getInstance().getPlayer(pl);
        if (splayer.getRank() == Rank.VIP || splayer.getRank() == Rank.PLUS || splayer.getRank() == Rank.MOD || splayer.getRank() == Rank.ADMIN || splayer.getRank() == Rank.OWNER) {
            return true;
        }
        return false;
    }

    /**
     * Gets if player is a donator
     * @param pl player to check
     * @return true or false
     */
    public static boolean hasPlusRank(Player pl) {
        NPlayer splayer = PlayerManager.getInstance().getPlayer(pl);
        if (splayer.getRank() == Rank.PLUS || splayer.getRank() == Rank.MOD || splayer.getRank() == Rank.ADMIN || splayer.getRank() == Rank.OWNER) {
            return true;
        }
        return false;
    }

    /**
     * Gets if player is a staff member
     * @param pl player to check
     * @return true or false
     */
    public static boolean hasStaffRank(Player pl) {
        NPlayer splayer = PlayerManager.getInstance().getPlayer(pl);
        if (splayer.getRank() == Rank.MOD || splayer.getRank() == Rank.ADMIN || splayer.getRank() == Rank.OWNER) {
            return true;
        }
        return false;
    }

    /**
     * Gets if player is a admin
     * @param pl player to check
     * @return true or false
     */
    public static boolean hasAdminRank(Player pl) {
        NPlayer splayer = PlayerManager.getInstance().getPlayer(pl);
        if (splayer.getRank() == Rank.OWNER || splayer.getRank() == Rank.ADMIN) {
            return true;
        }
        return false;
    }

    /**
     * gets full name of rank
     * @param rank rank to retrieve
     * @return full name
     */
    public String getFullName(Rank rank) {
        return rank.color + rank.name;
    }
}
