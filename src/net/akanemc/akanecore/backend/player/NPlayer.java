package me.xxmatthdxx.nebular.player;


import me.xxmatthdxx.nebular.Nebular;
import me.xxmatthdxx.nebular.rank.Rank;
import me.xxmatthdxx.nebular.rank.RankManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Scoreboard;

import java.util.UUID;

/**
 * Created by Matt on 02/06/2015.
 */
public class NPlayer {

    private Player player;
    private String lastName;
    private String currentName;
    private UUID uuid;
    private Rank rank;
    private String rankName;
    private String currentServer;
    private int coins;

    /**
     * Gets and returns a players rank.
     * @return The rank that a player has.
     */
    public Rank getRank() {
        return rank;
    }

    /**
     * Sets a players rank locally. Refer to PlayerManager and RankManager to change it in the DB.
     * @param rank The rank you want to give them.
     */
    public void setRank(Rank rank) {
        this.rank = rank;
    }

    /**
     * Gets the string value of the rank.
     * @return The rank name
     */
    public String getRankName() {
        return rankName;
    }

    /**
     * Sets the string value of the rank name.
     * @param rankName rank name to set.
     */

    public void setRankName(String rankName) {
        this.rankName = rankName;
    }

    /**
     * Gets the current server of the player.
     * @return the current servers name.
     */
    public String getCurrentServer() {
        return currentServer;
    }

    /**
     * Constructor
     * @param player Bukkit player to wrap.
     */
    public NPlayer(Player player){
        if(!player.getName().equals(lastName)){
            lastName = player.getName();
            currentName = player.getName();
        }

        uuid = player.getUniqueId();
        rank = RankManager.getInstance().getRank(player);
        rankName = RankManager.getInstance().getFullName(rank);
        currentServer = Bukkit.getServer().getServerName();
        coins = PlayerManager.getInstance().getCurrency(player);
    }

    /**
     * Gets the player we wrapped.
     * @return the Bukkit player.
     */
    public Player getPlayer() {
        return player;
    }

    /**
     * Sets the Bukkit player to wrap
     * @param player new Bukkit player to wrap.
     */
    public void setPlayer(Player player) {
        this.player = player;
    }

    /**
     * Gets the players last known name.
     * @return The players name
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets the players last name, created in case they change names.
     * @param lastName The new name we want to give them.
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Gets the current name the server knows.
     * @return Current name in memory and DB
     */
    public String getCurrentName() {
        return currentName;
    }

    /**
     * Sets the current name of the player
     * @param currentName Name to set
     */
    public void setCurrentName(String currentName) {
        this.currentName = currentName;
    }

    /**
     * Gets the players Unique Identifier.
     * @return The uuid of the player.
     */
    public UUID getUuid() {
        return uuid;
    }

    /**
     * Sets the uuid for the player.
     * @param uuid uuid we want to set theirs too.
     */
    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    /**
     * Send message to the player
     * @param message The message we want the player to receive.
     */
    public void sendMessage(String message) {
        player.sendMessage(Nebular.color(message));
    }

    /**
     * Gets the players rank prefix from memory.
     * @return String value for the prefix.
     */
    public String getRankPrefix() {
        return rank.getColor() + ChatColor.BOLD.toString() + rank.getName();
    }

    /**
     * Sets a players scoreboard.
     * @param board Scoreboard we want to set for them.
     */
    public void setScoreboard(Scoreboard board){
        player.setScoreboard(board);
    }

}
