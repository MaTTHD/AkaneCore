package me.xxmatthdxx.nebular.player;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import me.xxmatthdxx.nebular.Nebular;
import me.xxmatthdxx.nebular.punishments.PunishmentManager;
import me.xxmatthdxx.nebular.rank.Rank;
import me.xxmatthdxx.nebular.server.ServerManager;
import me.xxmatthdxx.nebular.stat.Stat;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * Created by Matt on 02/06/2015.
 */
public class PlayerManager {

    private static PlayerManager instance = new PlayerManager();

    public static PlayerManager getInstance() {
        return instance;
    }
    
    private List<Stat> pStats = new ArrayList<>();
    
    List<NPlayer> players = new ArrayList<>();

    /**
     * Loads and creates a instance of NPlayer by passing a player object
     * @param player player to wrap as a NPlayer
     */
    public void loadPlayer(Player player) {
        players.add(new NPlayer(player));
    }

    /**
     * Creates a new NPlayer in memory, called when players join, or whenever data could be lost.
     * @param player Player to wrap is a NPlayer
     */
    public void createNewPlayer(final Player player){
        Statement s = null;
        ResultSet set;
        try (Connection con = Nebular.getPlugin().getSql().openConnection()){
            s = con.createStatement();
            set = s.executeQuery("SELECT * FROM `playerData` WHERE uuid='" + player.getUniqueId() + "'");
            if(set.next()){
                loadPlayer(player);
            }
            else {
                s.executeUpdate(
                        "INSERT INTO `playerData` (`uuid`, `rank`, `coins`, `server`, `multiplier`, `bans`) VALUES ('" +player.getUniqueId() +
                                "', '" + Rank.DEFAULT.toString() + "', '" + 0 + "', '" +
                                Bukkit.getServer().getServerName() + "', '" + ServerManager.getInstance().getServer(Bukkit.getServer().getServerName()).getMultiplier() +
                                "', '" + "0')");

                new BukkitRunnable() {
                    public void run(){
                        loadPlayer(player);
                    }
                }.runTaskLater(Nebular.getPlugin(), 5L);
            }
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Unloads a NPlayer object from the collection.
     * @param player player to unload
     */
    public void unloadPlayer(Player player) {
        NPlayer pl = getPlayer(player);
        if (players.contains(pl)) {
            players.remove(pl);
        }
    }

    /**
     * Returns the NPlayer object that is wrapping the passed player
     * @param player Player we want to get the NPlayer object for
     * @return Returns the NPlayer object meant for the passed player
     */
    public NPlayer getPlayer(Player player) {
        for (NPlayer NPlayer : players) {
            if (NPlayer.getUuid() == player.getUniqueId()) {
                return NPlayer;
            }
        }
        return null;
    }

    /**
     * Gets the amount of currency a player has.
     * @param player Player to retrieve the currency for
     * @return Returns the amount of currency
     */
    public int getCurrency(Player player) {
        Statement s = null;
        ResultSet res = null;
        try (Connection con = Nebular.getPlugin().getSql().openConnection()) {
            s = con.createStatement();

            res = s.executeQuery("SELECT `coins` as c FROM `playerData` WHERE uuid='" + player.getUniqueId() + "'");
            if (res.next()) {
                if (res.getString("c") == null) {
                    return 0;
                } else {
                    return res.getInt("c");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return 0;
    }

    /**
     * Adds to a players existing balance
     * @param player Player to increase balance
     * @param amount The amount to increase it by.
     */
    public void addCurrency(Player player, int amount) {
        int current = getCurrency(player);

        int total = (current + amount);

        Statement s = null;

        try (Connection con = Nebular.getPlugin().getSql().openConnection()) {
            s = con.createStatement();
            s.executeUpdate("UPDATE playerData SET coins='" + total + "' WHERE uuid='" + player.getUniqueId() + "'");

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        System.out.println("Inserted info");
    }

    /**
     * Takes currency from the player.
     * @param player Player to take from.
     * @param amount Amount to take.
     */
    public void takeCurrency(Player player, int amount) {
        addCurrency(player, (-1 * amount));
    }
    
    public List<Stat> playerStats(){
    	return pStats;
    }
    
    public boolean isBanned(Player player) throws ParseException {
    	String end = PunishmentManager.getInstance().getEnd(player);
        DateFormat formatter = new SimpleDateFormat("MMMM:d:yyyy");
        Date end1 = formatter.parse(end);
        String now = formatter.format(new Date());
        Date now1 = formatter.parse(now);
        
        
        if(now1.compareTo(end1) < 0){
        	return true;
        }
        else {
        	return false;
        }
    }
}
