package me.xxmatthdxx.nebular.listeners;


import me.xxmatthdxx.nebular.Nebular;
import me.xxmatthdxx.nebular.player.PlayerManager;
import me.xxmatthdxx.nebular.server.ServerManager;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 * Created by Matt on 03/05/2015.
 */
public class PlayerQuit implements Listener {

    /**
     * Called when a player quits, to remove and or cache user data.
     * @param e
     */

    @EventHandler
    public void onQuit(PlayerQuitEvent e) {
        PlayerManager.getInstance().unloadPlayer(e.getPlayer());
        e.setQuitMessage(Nebular.color(Nebular.prefix + "&c " + e.getPlayer().getName() + " has disconnected."));

        ServerManager.getInstance().removePlayers(ServerManager.getInstance().getServer(Bukkit.getServer().getServerName()));
    }
}
