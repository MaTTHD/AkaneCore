package me.xxmatthdxx.nebular.listeners;


import me.xxmatthdxx.nebular.Nebular;
import me.xxmatthdxx.nebular.player.NPlayer;
import me.xxmatthdxx.nebular.player.PlayerManager;
import me.xxmatthdxx.nebular.rank.RankManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

/**
 * Created by Shiv on 6/14/2015.
 */
public class PlayerCommandPreprocess implements Listener {

    /**
     * Stops /plugins and /help to avoid spammy annoying bukkit commands
     */
    @EventHandler
    public void onPlayerCommand(PlayerCommandPreprocessEvent e) {
        final Player p = e.getPlayer();
        NPlayer pl = PlayerManager.getInstance().getPlayer(p);
        if (e.getMessage().startsWith("/plugins") || e.getMessage().startsWith("/plugins ") || e.getMessage().startsWith("/pl") || e.getMessage().startsWith("/pl ") || e.getMessage().startsWith("/bukkit:plugins") || e.getMessage().startsWith("/bukkit:plugins ")) {

            if (!RankManager.hasAdminRank(p)) {

                p.sendMessage(ChatColor.RED + "All you need to know is Nebular is enabled!");
                e.setCancelled(true);
                return;
            }
            e.setCancelled(false);
        }

        if (e.getMessage().startsWith("/help")  || e.getMessage().startsWith("/help ") || e.getMessage().startsWith("/bukkit:help") || e.getMessage().startsWith("/bukkit:help ")) {
            pl.sendMessage(Nebular.color(Nebular.prefix + "&eThis command is not available to public players &e&lyet&r&e."));
            e.setCancelled(true);
            // TODO MAKE MENU
        }

        if(e.getMessage().startsWith("/ban") || e.getMessage().startsWith("/ban ")){
            String[] args = e.getMessage().split(" ");
            // TODO ENTIRE CMD
        }
    }
}
