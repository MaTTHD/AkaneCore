package me.xxmatthdxx.nebular.listeners;

import me.xxmatthdxx.nebular.Nebular;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.FoodLevelChangeEvent;

/**
 * Created by Matt on 29/05/2015.
 */
public class FoodLevelChange implements Listener {
    /**
     * Cancels food level change, as long as it is set in the permission set.
     * @param e
     */
    private Nebular plugin = Nebular.getPlugin();

    @EventHandler
    public void change(FoodLevelChangeEvent e) {
        if (plugin.getSet().isFoodLoss()) {

        } else {
            e.setCancelled(true);
        }
    }
}
