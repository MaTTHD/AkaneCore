package me.xxmatthdxx.nebular.listeners;

import me.xxmatthdxx.nebular.Nebular;
import me.xxmatthdxx.nebular.player.NPlayer;
import me.xxmatthdxx.nebular.player.PlayerManager;
import me.xxmatthdxx.nebular.punishments.PunishmentManager;
import me.xxmatthdxx.nebular.rank.Rank;
import me.xxmatthdxx.nebular.rank.RankManager;
import me.xxmatthdxx.nebular.server.ServerManager;
import me.xxmatthdxx.nebular.utils.ScoreboardUtils;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * Created by Matt on 03/05/2015.
 */
public class PlayerJoin implements Listener {
	/**
	 * Join event listening for players joining and leaving.
	 * 
	 * @param e
	 */

	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		final Player player = e.getPlayer();
		e.setJoinMessage(null);

		PlayerManager.getInstance().createNewPlayer(player);

		NPlayer splayer = PlayerManager.getInstance().getPlayer(player);

		ServerManager.getInstance().addToPlayerCount(
				ServerManager.getInstance().getServer(
						Bukkit.getServer().getServerName()));

		if (splayer.getRank() == Rank.DEFAULT) {
			player.setPlayerListName(Nebular.color("&7" + player.getName()));
		} else {
			player.setPlayerListName(Nebular.color(splayer.getRank().getColor()
					+ splayer.getRankPrefix() + " " + ChatColor.RESET
					+ splayer.getRank().getColor() + splayer.getCurrentName()));
			// if name is more than 16+ characters, disable method.
		}
		if (ServerManager
				.getInstance()
				.getHubs()
				.contains(
						ServerManager.getInstance().getServer(
								Bukkit.getServerName()))) {
			ScoreboardUtils.setHubScoreboard(player);
		}
		else {
			
		}
	}
}
