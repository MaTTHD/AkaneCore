package me.xxmatthdxx.nebular.listeners;

import me.xxmatthdxx.nebular.Nebular;
import me.xxmatthdxx.nebular.player.NPlayer;
import me.xxmatthdxx.nebular.player.PlayerManager;
import me.xxmatthdxx.nebular.rank.Rank;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

/**
 * Created by Matt on 29/05/2015.
 */
public class PlayerChat implements Listener {
    private Nebular plugin = Nebular.getPlugin();

    /**
     * Simple Chat handler
     * @param e
     */

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onChat(AsyncPlayerChatEvent e) {
        if (plugin.getSet().isChatAllowed()) {
            NPlayer pl = PlayerManager.getInstance().getPlayer(e.getPlayer());

            String rawMessage = e.getMessage();
            String newMessage = rawMessage.replace("%", "%%");
            /**
             * Bukkit's weird chat formatting makes a single % as a place holder, so must change it to %%
             */

            if (pl.getRank() == Rank.DEFAULT) {
                /**
                 * Different chat format for Default ranks.
                 */
                e.setFormat(Nebular.color(pl.getRank().getColor() + " " + pl.getCurrentName() + "&6: &f" + newMessage));
            } else {
                e.setFormat(Nebular.color(pl.getRankPrefix() + " " + ChatColor.RESET + pl.getRank().getColor() + pl.getCurrentName() + "&6: &f" + newMessage));
                return;
            }
        }
        else {
            e.setCancelled(true);
        }
    }
}
