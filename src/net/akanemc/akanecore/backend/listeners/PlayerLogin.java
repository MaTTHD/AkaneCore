package me.xxmatthdxx.nebular.listeners;

import java.text.ParseException;

import me.xxmatthdxx.nebular.player.PlayerManager;
import me.xxmatthdxx.nebular.punishments.PunishmentManager;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;


public class PlayerLogin implements Listener {
	
	@EventHandler
	public void onLogin(PlayerLoginEvent e){
		Player pl = e.getPlayer();
		
		try {
			if(PlayerManager.getInstance().isBanned(pl)){
				pl.kickPlayer(ChatColor.translateAlternateColorCodes('&', PunishmentManager.getInstance().getReason(pl)));
			}
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

}
