package me.xxmatthdxx.nebularhub;

import me.xxmatthdxx.nebularhub.listeners.DamageEvent;
import me.xxmatthdxx.nebularhub.listeners.FoodLevelChange;
import me.xxmatthdxx.nebularhub.listeners.InteractEvent;
import me.xxmatthdxx.nebularhub.listeners.InvClick;
import me.xxmatthdxx.nebularhub.listeners.ItemDrop;
import me.xxmatthdxx.nebularhub.listeners.MobSpwn;
import me.xxmatthdxx.nebularhub.listeners.PlayerJoin;
import me.xxmatthdxx.nebularhub.listeners.PlayerQuit;
import me.xxmatthdxx.nebularhub.listeners.SignClick;
import me.xxmatthdxx.nebularhub.listeners.SignListener;
import me.xxmatthdxx.nebularhub.signs.SignManager;

import org.bukkit.Bukkit;
import org.bukkit.event.Listener;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created by Matt on 15/07/2015.
 */
public class NebularHub extends JavaPlugin implements Listener {

	private static NebularHub plugin;

    /**
     * Get input strewm
     * Input stream handle
     * Buffered reader
     * readline
     */

    public void onEnable() {
        PluginManager pm = Bukkit.getServer().getPluginManager();
        plugin = this;
        Bukkit.getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
        getConfig().options().copyDefaults(true);
        if (!getDataFolder().exists()) {
            getDataFolder().mkdir();
        }
        saveDefaultConfig();
        SignManager.getInstance().loadSigns();

        System.out.println(Bukkit.getServer().getServerName());
        pm.registerEvents(new SignListener(), this);
        pm.registerEvents(new SignClick(), this);
        pm.registerEvents(new PlayerJoin(), this);
        pm.registerEvents(new InteractEvent(), this);
        pm.registerEvents(new InvClick(), this);
        pm.registerEvents(new DamageEvent(), this);
        pm.registerEvents(new FoodLevelChange(), this);
        //pm.registerEvents(new BlockBreak(), this);
        pm.registerEvents(new PlayerQuit(), this);
        //pm.registerEvents(new BlockPlace(), this);
        pm.registerEvents(new ItemDrop(), this);
        pm.registerEvents(new MobSpwn(), this);
        
        SignManager.getInstance().loadSigns();

        SignManager.getInstance().start();
    }

    public void onDisable() {
        plugin = null;
    }

    public static NebularHub getPlugin() {
        return plugin;
    }
}
