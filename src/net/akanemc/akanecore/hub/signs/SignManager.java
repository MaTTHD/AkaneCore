package me.xxmatthdxx.nebularhub.signs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import me.xxmatthdxx.nebular.game.GameState;
import me.xxmatthdxx.nebular.server.HubServer;
import me.xxmatthdxx.nebular.server.PlayableServer;
import me.xxmatthdxx.nebular.server.ServerManager;
import me.xxmatthdxx.nebularhub.NebularHub;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Sign;
import org.bukkit.scheduler.BukkitRunnable;


/**
 * Created by Matthew on 2015-04-22.
 */
public class SignManager {

    List<Sign> signs = new ArrayList<>();
    HashMap<Sign, String> rawLineData = new HashMap<>();

    private static SignManager instance = new SignManager();

    public static SignManager getInstance() {
        return instance;
    }

    /*public Sign getSign(String server) {
        for (Sign sign : signs) {
            sign.update();
            if (sign.getLine(0) == null) {
                System.out.println(sign.getLine(0) + "NULL DEBUG");
            } else if (sign.getLine(0).split(" • ")[1] == null) {
                for (Map.Entry<Sign, String> signs : rawLineData.entrySet()) {
                    if (signs.getValue().equalsIgnoreCase(server)) {
                        return signs.getKey();
                    }
                }
            }
            System.out.println(sign.getLine(0) + " - DEBUG");
            if (ChatColor.stripColor(sign.getLine(0).split(" • ")[1]).equalsIgnoreCase(server)) {
                System.out.println(sign.getLine(0) + " - DEBUG");
                return sign;
            }
        }
        return null;
    }*/

    public void loadSigns() {
        for (String s : NebularHub.getPlugin().getConfig().getStringList("signs")) {
            String[] split = s.split(",");
            double x = Double.valueOf(split[0]);
            double y = Double.valueOf(split[1]);
            double z = Double.valueOf(split[2]);
            World world = Bukkit.getWorld(split[3]);
            Location loc = new Location(world, x, y, z);
            Sign sign = (Sign) loc.getWorld().getBlockAt(loc).getState();
            signs.add(sign);
        }
    }

    public HashMap<Sign, String> getRawLineData() {
        return rawLineData;
    }

    public String getRawServer(Sign sign) {
        return rawLineData.get(sign);
    }

    /*public String getServerFromSign(Sign sign) {
        sign.update();
        if (sign.getLine(0) == null) {
            System.out.println(sign.getLine(0) + "NULL DEBUG");
            return null;
        } else if (sign.getLine(0).split(" • ").length <= 1) {
            if (rawLineData.containsKey(sign)) {
                return rawLineData.get(sign);
            }
        }
        return ChatColor.stripColor(sign.getLine(0).split(" • ")[1
                ]);
    }*/

    public void addSign(Location loc) {
        if (!(loc.getWorld().getBlockAt(loc).getState() instanceof Sign)) {
            System.out.println("Not sign!!!!!");
            return;
        }
        signs.add((Sign) loc.getWorld().getBlockAt(loc).getState());

        List<String> signs = NebularHub.getPlugin().getConfig().getStringList("signs");
        signs.add(Double.toString(loc.getBlockX()) + "," + Double.toString(loc.getBlockY()) + "," + Double.toString(loc.getBlockZ()) + "," + loc.getWorld().getName());
        NebularHub.getPlugin().getConfig().set("signs", signs);
        NebularHub.getPlugin().saveConfig();
        NebularHub.getPlugin().reloadConfig();
        for (Sign sign : SignManager.getInstance().getAllSigns()) {
            SignManager.getInstance().updateSign(sign);
        }
        loadSigns();
    }

    public List<Sign> getAllSigns() {
        return signs;
    }

    public int getPlayersPlayingType(String type) {
        int amount = 0;
        for (PlayableServer server : ServerManager.getInstance().getAllServers()) {
            if (ServerManager.getInstance().getType(server) == null) {
                continue;
            }
            if (ServerManager.getInstance().getType(server).equalsIgnoreCase(type)) {
                amount += ServerManager.getInstance().getOnlinePlayers(server);
            }
        }
        return amount;
    }

    public void updateSign(final Sign sign) {
        String type = ChatColor.stripColor(sign.getLine(1));
        sign.setLine(0, sign.getLine(0));
        sign.setLine(1, sign.getLine(1));
        sign.setLine(2, "Players playing: " + getPlayersPlayingType(type));
        sign.setLine(3, "Click to Join");
        sign.update();
    }

    public void start() {
        new BukkitRunnable() {
            public void run() {
                for (Sign sign : signs) {
                    updateSign(sign);
                }
            }
        }.runTaskTimer(NebularHub.getPlugin(), 0, 10L);
    }

    public String getServerAvailible(String type) {
        for (PlayableServer server : ServerManager.getInstance().getAllServers()) {
            if (server instanceof HubServer) {
                continue;
            } else {
                System.out.println("TYPE " + server.getName() + " - " + ServerManager.getInstance().getType(server));
                if (ServerManager.getInstance().getType(server) == null) {
                    continue;
                }

                if (ServerManager.getInstance().getType(server).equalsIgnoreCase(type)) {
                    if (ServerManager.getInstance().getState(server) == GameState.JOIN) {
                        if (ServerManager.getInstance().getOnlinePlayers(server) < 12) {
                            return server.getName();
                        } else {
                            continue;
                        }
                    } else {
                        continue;
                    }
                } else {
                    continue;
                }
            }
        }
        return null;
    }
}