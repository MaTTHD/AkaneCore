package me.xxmatthdxx.nebularhub.listeners;

import me.xxmatthdxx.nebular.Nebular;
import me.xxmatthdxx.nebularhub.API.Title;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;



/**
 * Created by Matt on 29/05/2015.
 */
public class InvClick implements Listener {

    Location hunger_games = new Location(Bukkit.getWorld("world"), -22.724, 4.00000, -327.915, (float) -179.8, 0);
    Location ffa = new Location(Bukkit.getWorld("world"), -78.52, 4, -278, (float) 90, 0);
    Location hide_and_seek = new Location(Bukkit.getWorld("world"), -16.332, 4.00000, -208.700, 0, 0);
    Location skywars = new Location(Bukkit.getWorld("world"), -58.426, 76, -5921.227, 0, 0);


    @EventHandler
    public void onClick(InventoryClickEvent e) {
        Player pl = (Player) e.getWhoClicked();

        if (e.getCurrentItem().getItemMeta().getDisplayName().contains("HUNGER GAMES")) {
            pl.teleport(hunger_games);
            Title.sendTitle(pl, Nebular.color("&aClick a sign to play "), Nebular.color("&e&lHunger Games&a."), 20, 20, 20);
            pl.closeInventory();
            return;
        }

        if (e.getCurrentItem().getItemMeta().getDisplayName().contains("FREE FOR ALL")) {
            pl.teleport(ffa);
            Title.sendTitle(pl, Nebular.color("&aClick a sign to play "), Nebular.color("&e&lFree For All&a."), 20, 20, 20);
            pl.closeInventory();
            return;
        }

        if (e.getCurrentItem().getItemMeta().getDisplayName().contains("HIDE AND SEEK")) {
            pl.teleport(hide_and_seek);
            Title.sendTitle(pl, Nebular.color("&aClick a sign to play "), Nebular.color("&e&lHide and Seek&a."), 20, 20, 20);
            pl.closeInventory();
            return;
        }

        if (e.getCurrentItem().getItemMeta().getDisplayName().contains("SKYWARS")) {
        	pl.teleport(skywars);
            Title.sendTitle(pl, Nebular.color("&aClick a sign to play "), Nebular.color("&e&lSkywars&a."), 20, 20, 20);
            pl.closeInventory();
        }

        if (e.getCurrentItem().getItemMeta().getDisplayName().contains("HUB [1]")) {
            pl.sendMessage(Nebular.color("&bYou are already in &5&lHUB &7[&a&l1&7]&r&a."));
            pl.closeInventory();
        }

        if (e.getCurrentItem().getItemMeta().getDisplayName().contains("HATS")) {
            pl.sendMessage(Nebular.color("&cThe &6&lHATS &cfeature is currently disabled!"));
            pl.closeInventory();
            pl.playSound(pl.getLocation(), Sound.NOTE_SNARE_DRUM, 4, 4);
        }

        if (e.getCurrentItem().getItemMeta().getDisplayName().contains("ARMOR SELECTOR")) {
            pl.sendMessage(Nebular.color("&cThe &6&lARMOR SELECTOR &cfeature is currently disabled!"));
            pl.closeInventory();
            pl.playSound(pl.getLocation(), Sound.NOTE_SNARE_DRUM, 4, 4);
        }

        if (e.getCurrentItem().getItemMeta().getDisplayName().contains("FOUNTAIN")) {
            pl.sendMessage(Nebular.color("&cThe &6&lFOUNTAIN &cfeature is currently disabled!"));
            pl.closeInventory();
            pl.playSound(pl.getLocation(), Sound.NOTE_SNARE_DRUM, 4, 4);
        }

        if (e.getCurrentItem().getItemMeta().getDisplayName().contains("Hub 1")) {
            pl.sendMessage(Nebular.color("&dYou are already in this hub; join cancelled!"));
            pl.closeInventory();
            pl.playSound(pl.getLocation(), Sound.NOTE_BASS_DRUM, 4, 4);
        }

    }
}
