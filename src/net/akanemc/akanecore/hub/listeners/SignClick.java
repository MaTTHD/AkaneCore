package me.xxmatthdxx.nebularhub.listeners;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;

import me.xxmatthdxx.nebular.Nebular;
import me.xxmatthdxx.nebularhub.signs.SignManager;
import net.md_5.bungee.api.ChatColor;

import org.bukkit.Material;
import org.bukkit.block.Sign;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

import java.io.ByteArrayOutputStream;

/**
 * Created by Matt on 03/05/2015.
 */
public class SignClick implements Listener {

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onClick(PlayerInteractEvent e) {
        if (e.getAction() == Action.RIGHT_CLICK_BLOCK) {
            if (e.getClickedBlock().getType() == Material.SIGN_POST || e.getClickedBlock().getType() == Material.WALL_SIGN) {
                Sign sign = (Sign) e.getClickedBlock().getState();
                //This is a game sign
                String type = ChatColor.stripColor(sign.getLine(1));
            	System.out.println(type);
                String serverToTP = SignManager.getInstance().getServerAvailible(type);
                if(serverToTP == null){
                	e.getPlayer().sendMessage(ChatColor.RED + "Could not find a open server!");
                	return;
                }
                System.out.println(serverToTP);
                ByteArrayDataOutput out = ByteStreams.newDataOutput();
                out.writeUTF("Connect");
                out.writeUTF(serverToTP);
                e.getPlayer().sendPluginMessage(Nebular.getPlugin(), "BungeeCord", out.toByteArray());

            } else {
                return;
            }
        }
    }
}
