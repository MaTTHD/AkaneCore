package me.xxmatthdxx.nebularhub.listeners;

import me.xxmatthdxx.nebular.Nebular;
import me.xxmatthdxx.nebular.player.NPlayer;
import me.xxmatthdxx.nebular.player.PlayerManager;
import me.xxmatthdxx.nebular.rank.Rank;
import me.xxmatthdxx.nebular.rank.RankManager;
import me.xxmatthdxx.nebularhub.API.Title;
import me.xxmatthdxx.nebularhub.util.ItemUtil;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.scheduler.BukkitRunnable;


/**
 * Created by Matthew on 2015-05-10.
 */
public class PlayerJoin implements Listener {

    Location lobby = new Location(Bukkit.getWorld("Hub"), -29.095, 4.00000, -271.740, (float) -90.6, 0);

    public void onJoin(PlayerJoinEvent e) {
        final Player player = e.getPlayer();
        if (RankManager.getInstance().getRank(player) == null) {
            new BukkitRunnable() {
                public void run() {
                    RankManager.getInstance().setRank(player, Rank.DEFAULT);
                }
            }.runTaskLater(Nebular.getPlugin(), 5L);
        }
        PlayerManager.getInstance().loadPlayer(player);

        NPlayer splayer = PlayerManager.getInstance().getPlayer(e.getPlayer());

        PlayerInventory pi = e.getPlayer().getInventory();
        pi.clear();
        ItemStack serverselector = ItemUtil.createItem(Material.COMPASS, Nebular.color("&a&lGAME SELECTOR"));
        ItemStack hubselector = ItemUtil.createItem(Material.BEDROCK, Nebular.color("&a&lHUB SELECTOR"));
        ItemStack cosmetics = ItemUtil.createItem(Material.CHEST, Nebular.color("&a&lCOSMETICS"));
        pi.setItem(4, serverselector);
        pi.setItem(2, cosmetics);
        pi.setItem(6, hubselector);
        pi.setHeldItemSlot(4);
        Title.sendTitle(player, "&e&lWelcome to Survival Madness, ", " &a" + player.getName(), 20, 20, 20);
        player.setHealth(player.getMaxHealth());
        player.setFoodLevel(20);
        player.teleport(lobby);
        player.getInventory().setHeldItemSlot(4);
        player.sendMessage(Nebular.color("&6----------------------------------------"));
        player.sendMessage(Nebular.color("&aStore: &dstore.survivalmadness.net"));
        player.sendMessage(Nebular.color("&aOwners: &4LionMakerStudios &eand &4bmcc1234"));
        player.sendMessage(Nebular.color("&aDevelopers: &4bmcc1234 &eand &cTormatic"));
        player.sendMessage(Nebular.color("&6----------------------------------------"));
        if (splayer.getRank() == Rank.DEFAULT) {
            e.setJoinMessage(null);
        } else {
            e.setJoinMessage(Nebular.color(splayer.getRank().getColor() + splayer.getRankPrefix() + " " + ChatColor.RESET + splayer.getRank().getColor() + splayer.getCurrentName() + " &r&fhas joined the hub!"));
        }
    }
}
