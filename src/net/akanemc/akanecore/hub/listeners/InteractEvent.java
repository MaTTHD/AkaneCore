package me.xxmatthdxx.nebularhub.listeners;

import me.xxmatthdxx.nebular.Nebular;
import me.xxmatthdxx.nebularhub.util.ItemUtil;

import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
/**
 * Created by Matt on 29/05/2015.
 */
public class InteractEvent implements Listener {

    @EventHandler
    public void onClick(PlayerInteractEvent e) {

        Player p = e.getPlayer();

        if(e.getItem() == null){
            return;
        }

        if(e.getItem().getItemMeta() == null){
            return;
        }

        if(e.getItem().getItemMeta().getDisplayName() == null){
            return;
        }

        if (e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) {
            if (e.getItem().getItemMeta().getDisplayName().contains("HUB SELECTOR")) {
                Inventory hub_inv = Bukkit.createInventory(null, 9, Nebular.color("&a&lHUB: &f&LHUB SELECTOR"));
                ItemStack hub1 = ItemUtil.createLoreItem(Material.IRON_BLOCK, Nebular.color("&e&lHUB &7[&a&l1&7]"), Nebular.color("&5&lYOU ARE HERE."));
                hub_inv.setItem(4, hub1);
                p.openInventory(hub_inv);
            }
        }

        if (e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) {
            if (e.getItem().getItemMeta().getDisplayName().contains("GAME SELECTOR")) {
                Inventory game_inv = Bukkit.createInventory(null, 27, Nebular.color("&a&lHUB: &f&lSERVER SELECTOR"));
                ItemStack hunger_games = ItemUtil.createLoreItem(Material.DIAMOND_SWORD, Nebular.color("&E&lHUNGER GAMES &6- &b&LBETA"), Nebular.color("&7You vs. other players to be the last tribute alive."));
                ItemStack ffa = ItemUtil.createLoreItem(Material.IRON_CHESTPLATE, Nebular.color("&e&lFREE FOR ALL &6- &b&LBETA"), Nebular.color("&7Enter an awesome brawl against other players!"), "", Nebular.color("&7This mode is never ending!"));
                ItemStack hide_and_seek = ItemUtil.createLoreItem(Material.CLAY_BALL, Nebular.color("&e&lHIDE AND SEEK &6- &b&LBETA"), Nebular.color("&7Hide from the seekers!"));
                ItemStack survival = ItemUtil.createLoreItem(Material.GRASS, Nebular.color("&e&lSKYWARS &6- &b&lBETA"), Nebular.color("&7Battle it out on floating islands!"));
                game_inv.setItem(12, hunger_games);
                game_inv.setItem(13, ffa);
                game_inv.setItem(14, hide_and_seek);
                game_inv.setItem(22, survival);
                e.getPlayer().openInventory(game_inv);
            }
        }

        if (e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) {
            if (e.getItem().getItemMeta().getDisplayName().contains("COSMETICS")) {
                Inventory inv = Bukkit.getServer().createInventory(null, 9, Nebular.color("&a&LHUB: &f&lCOSMETIC MENU"));
                ItemStack hats = ItemUtil.createColoredItem(Material.LEATHER_HELMET, Nebular.color("&6&lHATS &a- &b&LVIP &eRANK EXCLUSIVE!"), Color.AQUA, Nebular.color("&ePurchase &b&lVIP &eon our store!"), Nebular.color("&dstore.minetrap.net"));
                ItemStack armor = ItemUtil.createColoredItem(Material.LEATHER_CHESTPLATE, Nebular.color("&6&lARMOR SELECTOR &a- &a&lELITE &eRANK EXCLUSIVE!"), Color.GREEN, Nebular.color("&ePurchase &a&lElite &eon our store!"), Nebular.color("&dstore.minetrap.net"));
                ItemStack fountain = ItemUtil.createLoreItem(Material.GOLD_NUGGET, Nebular.color("&6&lFOUNTAIN O' GOLD &a- &a&lELITE &eRANK EXCLUSIVE!"), Nebular.color("&ePurchase &a&lElite &eon our store!"), Nebular.color("&dstore.minetrap.net"));
                inv.setItem(3, hats);
                inv.setItem(4, armor);
                inv.setItem(5, fountain);
                p.openInventory(inv);
            }
        }
    }
}
