package me.xxmatthdxx.nebularhub.listeners;

import me.xxmatthdxx.nebular.server.PlayableServer;
import me.xxmatthdxx.nebular.server.ServerManager;
import me.xxmatthdxx.nebularhub.NebularHub;
import me.xxmatthdxx.nebularhub.signs.SignManager;

import org.bukkit.ChatColor;
import org.bukkit.block.Sign;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * Created by Matt on 03/05/2015.
 */
public class SignListener implements Listener {

	@EventHandler
	public void onSignPlace(final SignChangeEvent e) {
		if (!SignManager.getInstance().getAllSigns()
				.contains(e.getBlock().getState())) {

			SignManager
					.getInstance()
					.getRawLineData()
					.put((Sign) e.getBlock().getState(),
							ChatColor.stripColor(e.getLine(1)));
			String type = ChatColor.stripColor(e.getLine(0));

			if (type.equalsIgnoreCase("solo") || type.equalsIgnoreCase("team") || type.equalsIgnoreCase("Skywars")) {
				e.setLine(0, ChatColor.GREEN + ChatColor.BOLD.toString()
						+ "Skywars");
				e.setLine(1, ChatColor.GREEN + ChatColor.BOLD.toString() +type);
				e.setLine(2, "Players playing: "
						+ SignManager.getInstance().getPlayersPlayingType(type));
				e.setLine(3, "Click to Join");
				System.out.println(e.getLine(2));
				new BukkitRunnable(){
					public void run(){
						SignManager.getInstance().addSign(e.getBlock().getLocation());
						SignManager.getInstance().updateSign((Sign) e.getBlock().getState());
					}
				}.runTaskLater(NebularHub.getPlugin(), 3*20L);
			} else {
				return;
			}
		}
	}
}
