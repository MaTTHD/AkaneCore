package net.akanemc.akanecore;

import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Matthew on 2015-12-04.
 */
public class AkaneCore extends JavaPlugin {

    private static AkaneCore instance;

    private List<AkaneModule> modules = new ArrayList<>();

    public void onEnable(){
        instance = this;
    }

    public void onDisable(){
        instance = null;
    }

    public static AkaneCore getInstance(){
        return instance;
    }
}
